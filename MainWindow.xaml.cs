﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.DepthBasics
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Microsoft.Kinect;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Map depth range to byte range
        /// </summary>
        private const int MapDepthToByte = 8000 / 256;
        
        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Reader for depth frames
        /// </summary>
        private DepthFrameReader depthFrameReader = null;

        /// <summary>
        /// Description of the data contained in the depth frame
        /// </summary>
        private FrameDescription depthFrameDescription = null;
            
        /// <summary>
        /// Bitmap to display
        /// </summary>
        private WriteableBitmap depthBitmap = null;

        /// <summary>
        /// Intermediate storage for frame data converted to color
        /// </summary>
        private byte[] depthPixels = null;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            // get the kinectSensor object
            this.kinectSensor = KinectSensor.GetDefault();

            // open the reader for the depth frames
            this.depthFrameReader = this.kinectSensor.DepthFrameSource.OpenReader();

            // wire handler for frame arrival
            this.depthFrameReader.FrameArrived += this.Reader_FrameArrived;

            // get FrameDescription from DepthFrameSource
            this.depthFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;

            // allocate space to put the pixels being received and converted
            this.depthPixels = new byte[this.depthFrameDescription.Width * this.depthFrameDescription.Height];           

            // create the bitmap to display
            this.depthBitmap = new WriteableBitmap(this.depthFrameDescription.Width, this.depthFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray8, null);

            // set IsAvailableChanged event notifier
            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            // open the sensor
            this.kinectSensor.Open();

            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.NoSensorStatusText;

            // use the window object as the view model in this simple example
            this.DataContext = this;

            // initialize the components (controls) of the window
            this.InitializeComponent();
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.depthBitmap;
            }
        }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.depthFrameReader != null)
            {
                // DepthFrameReader is IDisposable
                this.depthFrameReader.Dispose();
                this.depthFrameReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        /// <summary>
        /// Handles the user clicking on the screenshot button
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void ScreenshotButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.depthBitmap != null)
            {
                // create a png bitmap encoder which knows how to save a .png file
                BitmapEncoder encoder = new PngBitmapEncoder();

                // create frame from the writable bitmap and add to encoder
                encoder.Frames.Add(BitmapFrame.Create(this.depthBitmap));

                string time = System.DateTime.UtcNow.ToString("hh'-'mm'-'ss", CultureInfo.CurrentUICulture.DateTimeFormat);

                string myPhotos = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

                string path = Path.Combine(myPhotos, "KinectScreenshot-Depth-" + time + ".png");

                // write the new file to disk
                try
                {
                    // FileStream is IDisposable
                    using (FileStream fs = new FileStream(path, FileMode.Create))
                    {
                        encoder.Save(fs);
                    }

                    this.StatusText = string.Format(CultureInfo.CurrentCulture, Properties.Resources.SavedScreenshotStatusTextFormat, path);
                }
                catch (IOException)
                {
                    this.StatusText = string.Format(CultureInfo.CurrentCulture, Properties.Resources.FailedScreenshotStatusTextFormat, path);
                }
            }
        }

        // Chunji 05/29/2015 Method to calculate camera tilt angle
        // output int in degrees        
        private double GetCameraTiltAngle(ushort[] depthArray){
            // Height and Width of depth frame
            int width = this.depthFrameDescription.Width;
            int height = this.depthFrameDescription.Height;
            // Area used for calculating
            int htop = 100;
            int hbottom = height/2;     // only take top half of depth frame for calculation
            int h = hbottom - htop;
            int w = 200;
            int wleft = width/2 - w/2;
            int wright = width/2 + w/2;

            // Averaged depth value of each line
            int i;
            int j;
            double[] aveline = new double[height];          // average of depth value of all lines
            for (i = htop; i <= hbottom; i++)
            {
                int sumline = 0;    // sum of depth value of each line
                for (j = wleft; j < wright; j++)
                {
                    sumline += depthArray[i * width + j];
                }
                aveline[i] = (double)sumline / w;
            }

            // check data validity
            double sum = 0;
            double sumsq = 0;            
            for (i = htop; i <= hbottom; i++)
            {
                for (j = wleft; j < wright; j++)
                {
                    sum += depthArray[i * width + j];
                    sumsq += depthArray[i * width + j] * depthArray[i * width + j];
                }
            }
            int N = (hbottom - htop + 1) * (wright - wleft);
            double ave = sum / N;
            double sumsqrave = sumsq / N;
            double std = Math.Sqrt(sumsqrave - ave * ave);
            
            // Rescale vertical field of view
            double topTheta = this.depthFrameDescription.VerticalFieldOfView / 2;
            double tanTheta = Math.Tan(topTheta * Math.PI / 180) * h / (height / 2);

            // Distance of the camera (mm) to the wall at the center of the depth frame
            double distanceCenter = (aveline[hbottom] + aveline[hbottom - 1]) / 2;
            // Distance of top margin (mm) to the center of the depth frame, measured horizontally in the view of the camera.
            double distanceHorizontal = aveline[htop] - aveline[hbottom];
            // Distance of top margin (mm) to the center of the depth frame, measured vertically in the view of the camera.
            double distanceVertical = (distanceCenter - distanceHorizontal) * tanTheta;
            
            // Tilt angle of the camera            
            double angle = Math.Atan(distanceHorizontal / distanceVertical) * 180 / Math.PI;
            return -(int)angle;
            //return std;
            
        } 
        // End


        /// <summary>
        /// Handles the depth frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_FrameArrived(object sender, DepthFrameArrivedEventArgs e)
        {
            bool depthFrameProcessed = false;

            using (DepthFrame depthFrame = e.FrameReference.AcquireFrame())
            {
                if (depthFrame != null)
                {
                    // the fastest way to process the body index data is to directly access 
                    // the underlying buffer
                    using (Microsoft.Kinect.KinectBuffer depthBuffer = depthFrame.LockImageBuffer())
                    {
                        /*
                        // Chunji depthFrameDescription are a few constants.
                        // BytesPerPixel = 2; ushort16.
                        // Width = 512; Height = 424.
                        // LengthInPixels = 512 * 424.
                        labelCameraTilt.Content = string.Format(
                            "Width: {0}, Height: {1}, BytesPerPixel: {2}, Diagonal: {3}, Vertical: {4}, Horizontal: {5}, LengthInPixels: {6}.", 
                            this.depthFrameDescription.Width,
                            this.depthFrameDescription.Height,
                            this.depthFrameDescription.BytesPerPixel,
                            this.depthFrameDescription.DiagonalFieldOfView,
                            this.depthFrameDescription.VerticalFieldOfView,
                            this.depthFrameDescription.HorizontalFieldOfView,
                            this.depthFrameDescription.LengthInPixels);
                        // End 
                        
                        // Chunji DepthFrame.LockImageBuffer()
                        labelCameraTilt.Content = string.Format(
                            "UnderlyingBuffer: {0}, Size: {1}",
                            depthBuffer.UnderlyingBuffer,
                            depthBuffer.Size);
                        // End
                        // Chunji DepthFrame.CopyFrameDataToArray()
                        ushort[] myDepthArray = new ushort[512*424];
                        depthFrame.CopyFrameDataToArray(myDepthArray);
                        labelCameraTilt.Content = string.Format(
                            "DepthArray: {0}", myDepthArray[512*424-2]);
                        // End
                         */ 
                        // Chunji Camera Tilt Angle
                        ushort[] myDepthArray = new ushort[512*424];
                        depthFrame.CopyFrameDataToArray(myDepthArray);
                        labelCameraTilt.Content = string.Format(
                            "Camera Tilt Angle: {0}", GetCameraTiltAngle(myDepthArray));
                        //labelCameraTilt2.Content = string.Format(
                        //    "Camera Tilt Angle: {0}", GetCameraTiltAngle(myDepthArray));
                        // End
                        
                        // verify data and write the color data to the display bitmap
                        if (((this.depthFrameDescription.Width * this.depthFrameDescription.Height) == (depthBuffer.Size / this.depthFrameDescription.BytesPerPixel)) &&
                            (this.depthFrameDescription.Width == this.depthBitmap.PixelWidth) && (this.depthFrameDescription.Height == this.depthBitmap.PixelHeight))
                        {
                            // Note: In order to see the full range of depth (including the less reliable far field depth)
                            // we are setting maxDepth to the extreme potential depth threshold
                            ushort maxDepth = ushort.MaxValue;

                            // If you wish to filter by reliable depth distance, uncomment the following line:
                            //// maxDepth = depthFrame.DepthMaxReliableDistance
                            
                            this.ProcessDepthFrameData(depthBuffer.UnderlyingBuffer, depthBuffer.Size, depthFrame.DepthMinReliableDistance, maxDepth);
                            depthFrameProcessed = true;
                        }
                    }
                }
            }

            if (depthFrameProcessed)
            {
                this.RenderDepthPixels();
            }
        }

        /// <summary>
        /// Directly accesses the underlying image buffer of the DepthFrame to 
        /// create a displayable bitmap.
        /// This function requires the /unsafe compiler option as we make use of direct
        /// access to the native memory pointed to by the depthFrameData pointer.
        /// </summary>
        /// <param name="depthFrameData">Pointer to the DepthFrame image data</param>
        /// <param name="depthFrameDataSize">Size of the DepthFrame image data</param>
        /// <param name="minDepth">The minimum reliable depth value for the frame</param>
        /// <param name="maxDepth">The maximum reliable depth value for the frame</param>
        private unsafe void ProcessDepthFrameData(IntPtr depthFrameData, uint depthFrameDataSize, ushort minDepth, ushort maxDepth)
        {
            // depth frame data is a 16 bit value
            ushort* frameData = (ushort*)depthFrameData;

            // Chunji
            // labelCameraTilt.Content = string.Format("Camera Tilt Angle: {0}", frameData[10000]);            
            // End

            // convert depth to a visual representation
            for (int i = 0; i < (int)(depthFrameDataSize / this.depthFrameDescription.BytesPerPixel); ++i)
            {
                // Get the depth for this pixel
                ushort depth = frameData[i];

                // To convert to a byte, we're mapping the depth value to the byte range.
                // Values outside the reliable depth range are mapped to 0 (black).
                this.depthPixels[i] = (byte)(depth >= minDepth && depth <= maxDepth ? (depth / MapDepthToByte) : 0);
            }
        }

        /// <summary>
        /// Renders color pixels into the writeableBitmap.
        /// </summary>
        private void RenderDepthPixels()
        {
            this.depthBitmap.WritePixels(
                new Int32Rect(0, 0, this.depthBitmap.PixelWidth, this.depthBitmap.PixelHeight),
                this.depthPixels,
                this.depthBitmap.PixelWidth,
                0);
        }

        /// <summary>
        /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            // on failure, set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.SensorNotAvailableStatusText;
        }
    }
}
